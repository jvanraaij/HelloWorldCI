﻿using System;

namespace HelloProviderLibrary
{
    public class HelloProvider
    {
        /// <summary>
        /// Returns the hello world string.
        ///
        /// This is really just an excuse to have a simple library in this test solution.
        /// </summary>
        /// <returns>Hello World</returns>
        public string GetHello()
        {
            return "Hello World";
        }
    }
}