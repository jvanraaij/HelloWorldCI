using System;
using HelloProviderLibrary;
using Xunit;

namespace HelloProviderTests
{
    public class HelloProviderTest
    {
        [Fact]
        public void SaysHelloToWorld()
        {
            var provider = new HelloProvider();
            Assert.Equal("Hello World", provider.GetHello());
        }
    }
}