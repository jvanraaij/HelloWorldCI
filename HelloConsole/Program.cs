﻿using System;
using HelloProviderLibrary;

namespace HelloConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var provider = new HelloProvider();

            var appName = typeof(Program).Assembly.FullName;
            var providerName = provider.GetType().Assembly.FullName;

            Console.WriteLine(provider.GetHello());
            Console.WriteLine($"Names: app={appName}; provider={providerName}");
        }
    }
}